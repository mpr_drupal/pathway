<?php

namespace Drupal\srgimporter\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\Entity\Term;


/**
 * Controller routines for contentimport routes.
 */
class SRGImportController extends ControllerBase {

  /**
   * Get All Content types.
   */
  public static function getAllContentTypes() {
    $contentTypes = \Drupal::service('entity.manager')->getStorage('node_type')->loadMultiple();
    $contentTypesList = [];
    $contentTypesList['none'] = 'Select';
    foreach ($contentTypes as $contentType) {
      $contentTypesList[$contentType->id()] = $contentType->label();
    }
    return $contentTypesList;
  }
  /**
   * Get All vacabularies
   */
    public static function getAllVacabularies($filterBy) {
      $vocabulary = \Drupal::service('entity.manager')->getStorage('taxonomy_term')->loadMultiple();
      $valList =[];
      $final = [];
      foreach ($vocabulary as $v) {
         $valList[$v->id()]['bundle'] = $v->bundle();
         $valList[$v->id()]['label'] = $v->label();
      }

      $new = array_filter($valList, function ($var) use ($filterBy) {

        return ($var['bundle'] == $filterBy);
       });

       $namearray = array();
       $namearray['none'] = 'Select';

       foreach ($new as $item) {

           $namearray[$item['label']] = $item['label'];
       }

      return $namearray;

    }

    /*
    * Get Study by SharepointID
    */

    public static function getNodesByTitle($title){
      $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['title' => $title]);
      return reset($nodes);
    }
    public static function getStudiesByTitleAndModel($title, $model){
      $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['title' => $title]);
      foreach($nodes as $study){

        if ($study->get('field_model')->target_id->value == $model){
          return $study;
        }
      }
      return ;
    }

    public static function deleteNodesByTitle($title){
      $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['title' => $title]);
      $node=reset($nodes);
      node_delete($node->nid->value);
    }


  /**
   * To get all Content Type Fields.
   */
  public static function getFields($contentType)
  {
    $fields = [];
    foreach (\Drupal::entityManager()
      ->getFieldDefinitions('node', $contentType) as $field_definition) {
      if (!empty($field_definition->getTargetBundle())) {
        $fields['name'][] = $field_definition->getName();
        $fields['type'][] = $field_definition->getType();
        $fields['setting'][] = $field_definition->getSettings();
      }
    }
    return $fields;
  }

  /**
   * To get Reference field ids.
   */
  public static function getTermReference($voc, $terms)
  {
    $vocName = strtolower($voc);
    $vid = preg_replace('@[^a-z0-9_]+@', '_', $vocName);
    $vocabularies = Vocabulary::loadMultiple();
    /* Create Vocabulary if it is not exists */
    if (!isset($vocabularies[$vid])) {
      SRGImportController::createVoc($vid, $voc);
    }
    $termArray = array_map('trim', explode(';', $terms));
    $termIds = [];
    foreach ($termArray as $term) {
      $term_id = SRGImportController::getTermId($term, $vid);
      if (empty($term_id) && $term !== '') {
        $term_id = SRGImportController::createTerm($voc, $term, $vid);
      }
      if (!empty($term_id)) $termIds[]['target_id'] = $term_id;
    }
    return $termIds;
  }

  /**
   * To Create Terms if it is not available.
   */
  public static function createVoc($vid, $voc)
  {
    $vocabulary = Vocabulary::create([
      'vid' => $vid,
      'machine_name' => $vid,
      'name' => $voc,
    ]);
    $vocabulary->save();
  }

  /**
   * To Create Terms if it is not available.
   */
  public static function createTerm($voc, $term, $vid)
  {
    Term::create([
      'parent' => [$voc],
      'name' => $term,
      'vid' => $vid,
    ])->save();
    $termId = SRGImportController::getTermId($term, $vid);
    return $termId;
  }

  /**
   * To get Termid available.
   */
  public static function getTermId($term, $vid)
  {
    $termRes = db_query('SELECT n.tid FROM {taxonomy_term_field_data} n WHERE n.name  = :uid AND n.vid  = :vid', [':uid' => $term, ':vid' => $vid]);
    foreach ($termRes as $val) {
      $term_id = $val->tid;
    }
    return $term_id;
  }

  /**
   * To get node available.
   */
  public static function getNodeId($title)
  {
    $nodeReference = [];
    $db = \Drupal::database();
    foreach ($title as $key => $value) {
      $query = $db->select('node_field_data', 'n');
      $query->fields('n', ['nid']);
      $nodeId = $query
        ->condition('n.title', trim($value))
        ->execute()
        ->fetchField();
      $nodeReference[$key]['target_id'] = $nodeId;
    }
    return $nodeReference;
  }

  /**
   * To get user information based on emailIds.
   */
  public static function getUserInfo($userArray)
  {
    $uids = [];
    foreach ($userArray as $usermail) {
      if (filter_var($usermail, FILTER_VALIDATE_EMAIL)) {
        $users = \Drupal::entityTypeManager()->getStorage('user')
          ->loadByProperties([
            'mail' => $usermail,
          ]);
      } else {
        $users = \Drupal::entityTypeManager()->getStorage('user')
          ->loadByProperties([
            'name' => $usermail,
          ]);
      }
      $user = reset($users);
      if ($user) {
        $uids[] = $user->id();
      } else {
        $user = User::create();
        $user->uid = '';
        $user->setUsername($usermail);
        $user->setEmail($usermail);
        $user->set("init", $usermail);
        $user->enforceIsNew();
        $user->activate();
        $user->save();
        $users = \Drupal::entityTypeManager()->getStorage('user')
          ->loadByProperties(['mail' => $usermail]);
        $uids[] = $user->id();
      }
    }
    return $uids;
  }

}
