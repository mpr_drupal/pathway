<?php

namespace Drupal\srgimporter\Form;

use Drupal\srgimporter\Controller\SRGImportController;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;


use PhpOffice\PhpSpreadsheet\IOFactory;


class SRGImporter extends ConfigFormBase
{
  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'srgimporter';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return [
      'srgimporter.settings',
    ];
  }

  /**
   * SRG Import Form.
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {

    $form['file_name'] = array(
      '#type' => 'textfield',
      '#title' => $this
        ->t('File path/name'),
      '#size' => 60,
      '#maxlength' => 300,
      '#required' => TRUE,
    );


    $form['file_upload'] = [
      '#type' => 'file',
      '#title' => $this->t('Import Study/Study Outcome from XLSX File'),
      '#size' => 40,
      '#description' => $this->t('Select the XLSX file to be imported.'),
      '#required' => false,
      '#autoupload' => true,
      '#upload_validators' => ['file_validate_extensions' => ['XLSX']],
    ];


    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
      '#button_type' => 'primary',
    ];
    return $form;
  }
  /**
   * SRG Import Form Submission.
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {

    $name =$form_state->getValue('file_name');
    $inputFileType = 'Xlsx';


    $inputFileType = 'Xlsx';
    $inputFileName = $_FILES['files']['tmp_name']['file_upload'];
    $sheetstudy = 'StudyDetail-NF';
    $sheetfinding = 'Findings-NF';

    $sheetmanuscripts = 'Manuscripts-NF';

    $reader = IOFactory::createReader($inputFileType);

    $spreadsheet = $reader->load($inputFileName);
    $studydata = $spreadsheet->getSheetByName($sheetstudy)->toArray(null, true, true, true);
    $findingdata = $spreadsheet->getSheetByName($sheetfinding)->toArray(null, true, true, true);
    $manuscriptsdata = $spreadsheet->getSheetByName($sheetmanuscripts)->toArray(null, true, true, true);

    drupal_flush_all_caches();
    global $base_url;


  SRGImporter::createNodeFromXLSX($manuscriptsdata, 'manuscripts', '');
    SRGImporter::createNodeFromXLSX($studydata, 'study',$name);
    SRGImporter::createNodeFromXLSX($findingdata, 'outcomes', '');


    $url = $base_url . "/admin/content";
    header('Location:' . $url);
    exit;
  }



  public function createNodeFromXLSX($data, $contentType, $filelocation)
  {

    $logFileName = $contentType . '.' . 'txt';
    $fields = SRGImportController::getFields($contentType);
    $fieldNames = $fields['name'];

    $fieldTypes = $fields['type'];
    $fieldSettings = $fields['setting'];


    $fieldTypes = SRGImporter::keyRename($fieldTypes, $fieldNames);
    $fieldSettings = SRGImporter::keyRename($fieldSettings, $fieldNames);


    $logVariationFields = '';
    $keyIndex = [];

    $table = [];


    $firstrow = [];
    $i = 0;
    foreach ($data  as $r) {
      if ($i == 0) {
        $firstrow = $r;
        printf("<table>\n");
      } else {
        // printf("<tr>");
        //print_r( $r);
        $row = [];
        foreach ($r as $k => $v) {

          $cn = $firstrow[$k];


          $row[$cn] = $v;
        }

        $logFile = fopen("sites/default/files/" . $logFileName, "w") or die("There is no permission to create log file. Please give permission for sites/default/file!");


        $table[$i] =  $row;
      }

      $i++;
    }

    // return $table;
    foreach ($table as $data) {
      foreach ($data as $key => $val) {
        if ($key === 'title' || in_array($key, $fieldNames)) {
          switch ($fieldTypes[$key]) {
            case 'image':
              $logVariationFields .= "Importing Image (" . trim($val) . ") :: ";
              if (!empty($val)) {
                $imgIndex = trim($val);
                $files = glob('sites/default/files/' . $contentType . '/images/' . $imgIndex);
                $fileExists = file_exists('sites/default/files/' . $imgIndex);
                if (!$fileExists) {
                  $images = [];
                  foreach ($files as $file_name) {
                    $image = File::create(['uri' => 'public://' . $contentType . '/images/' . basename($file_name)]);
                    $image->save();
                    $images[basename($file_name)] = $image;
                    $imageId = $images[basename($file_name)]->id();
                    $imageName = basename($file_name);
                  }
                  $nodeArray[$key] = [
                    [
                      'target_id' => $imageId,
                      'alt' => $nodeArray['title'],
                      'title' => $nodeArray['title'],
                    ],
                  ];
                  $logVariationFields .= "Image uploaded successfully \n ";
                }
              }
              $logVariationFields .= " Success \n";
              break;

            case 'entity_reference':
              $logVariationFields .= "Importing Reference Type (" . $fieldSettings[$key]['target_type'] . ") :: ";
              if ($fieldSettings[$key]['target_type'] == 'taxonomy_term') {
                $term_name = reset($fieldSettings[$key]['handler_settings']['target_bundles']);
                $terms = SRGImportController::getTermReference($term_name, $val);
                $nodeArray[$key] = $terms;
              } elseif ($fieldSettings[$key]['target_type'] == 'user') {
                $userArray = explode(', ', $val);
                $users = SRGImportController::getUserInfo($userArray);
                $nodeArray[$key] = $users;
              } elseif ($fieldSettings[$key]['target_type'] == 'node') {
                $nodeArrays = explode(';', $val);
                $nodeReference1 = SRGImportController::getNodeId($nodeArrays);
                $nodeArray[$key] = $nodeReference1;
              }
              $logVariationFields .= " Success \n";
              break;


            case 'text':
              $logVariationFields .= "Importing Content (" . $key . ") :: ";
              $nodeArray[$key] = [
                'value' => $val,
                'format' => 'restricted_html',
              ];
              $logVariationFields .= " Success \n";
              break;

            case 'entity_reference_revisions':
            case 'text_long':
            case 'text_with_summary':
              $logVariationFields .= "Importing Content (" . $key . ") :: ";
              $nodeArray[$key] = [

                'value' => $val,
                'format' => 'full_html',
              ];
              $logVariationFields .= " Success \n";

              break;

            case 'datetime':
              $logVariationFields .= "Importing Datetime (" . $key . ") :: ";
              $dateArray = explode(':', $val);
              if (count($dateArray) > 1) {
                $dateTimeStamp = strtotime($val);
                $newDateString = date('Y-m-d\TH:i:s', $dateTimeStamp);
              } else {
                $dateTimeStamp = strtotime($val);
                $newDateString = date('Y-m-d', $dateTimeStamp);
              }
              $nodeArray[$key] = ["value" => $newDateString];
              $logVariationFields .= " Success \n";
              break;

            case 'timestamp':
              $logVariationFields .= "Importing Content (" . $key . ") :: ";
              $nodeArray[$key] = ["value" => $val];
              $logVariationFields .= " Success \n";
              break;

            case 'boolean':
              $logVariationFields .= "Importing Boolean (" . $key . ") :: ";
              $nodeArray[$key] = $val;
              $logVariationFields .= " Success \n";
              break;

            case 'langcode':
              $logVariationFields .= "Importing Langcode (" . $key . ") :: ";
              $nodeArray[$key] = ($val != '') ? $val : 'en';
              $logVariationFields .= " Success \n";
              break;

            case 'geolocation':
              $logVariationFields .= "Importing Geolocation Field (" . $key . ") :: ";
              $geoArray = explode(";", $val);
              if (count($geoArray) > 0) {
                $geoMultiArray = [];
                for ($g = 0; $g < count($geoArray); $g++) {
                  $latlng = explode(",", $geoArray[$g]);
                  for ($l = 0; $l < count($latlng); $l++) {
                    $latlng[$l] = floatval(preg_replace("/\[^0-9,.]/", "", $latlng[$l]));
                  }
                  array_push($geoMultiArray, [
                    'lat' => $latlng[0],
                    'lng' => $latlng[1],
                  ]);
                }
                $nodeArray[$key] = $geoMultiArray;
              } else {
                $latlng = explode(",", $val);
                for ($l = 0; $l < count($latlng); $l++) {
                  $latlng[$l] = floatval(preg_replace("/\[^0-9,.]/", "", $latlng[$l]));
                }
                $nodeArray[$key] = ['lat' => $latlng[0], 'lng' => $latlng[1]];
              }
              $logVariationFields .= " Success \n";
              break;

            case 'entity_reference_revisions':

              break;

            default:
              $nodeArray[$key] = $val;
              break;
          }
        }
      }
      $nodeArray['langcode'] = 'en';

      $nodeArray['type'] = strtolower($contentType);
      $nodeArray['uid'] = 1;
      $nodeArray['promote'] = 0;
      $nodeArray['sticky'] = 0;
      $nodeArray['status'] = 1;

      if ($nodeArray['type']=='study'){
        $nodeArray['field_srg_file_path']=$filelocation;
      }

      if ($nodeArray['title'] != '') {
        //$nodeArray['title']=strip_tags($nodeArray['title']);
        $node = Node::create($nodeArray);


        $node->save();
        $logVariationFields .= "********************* Node Imported successfully ********************* \n\n";
        fwrite($logFile, $logVariationFields);
      }
      $nodeArray = [];
      // return $nodeArray;
    }  //loop through $table
    // return $node;


  }

  private function keyRename(array $hash, array $replacements)
  {

    foreach ($hash as $k => $v) {
      $hash[$replacements[$k]] = $v;
      unset($hash[$k]);
    }

    return $hash;
  }
}
