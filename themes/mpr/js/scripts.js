
jQuery(function(){
	
	jQuery("#need").click(function () {
 		 jQuery('path').each(function (index, value) { 
				if ( jQuery(this).hasClass('need-Yes')) {
			 		jQuery(this).addClass('active-map');
				}
  			else jQuery(this).removeClass('active-map') ;
		});
	});
	 jQuery("#fin").click(function () {
 		 jQuery('path').each(function (index, value) { 
				if ( jQuery(this).hasClass('fin-Yes')) {
			 		jQuery(this).addClass('active-map');
				}
  			else jQuery(this).removeClass('active-map') ;
		});
	});
	
	jQuery("#long").click(function () {
 		 jQuery('path').each(function (index, value) { 
				if ( jQuery(this).hasClass('long-Yes')) {
			 		jQuery(this).addClass('active-map');
				}
  			else jQuery(this).removeClass('active-map') ;
		});
	});
	

	
/**	jQuery(document).ready(function() {
		
	
		
  // The link has class="linky-link" for easy selecting and href="http://www.page-2.com".  I did not use an id because I had multiple nearly identical links on the same page.
  jQuery("#comment-form button.js-form-submit").on("click", function(e) {
    e.preventDefault();  // Don't redirect yet!
    var $form = jQuery("#comment-form");  // $form instead of form because it's shorthand for a jQuery selector - not strictly necessary, but helpful
    $form.append("<input type='hidden' name='link_redirect' value='" + GetURLParameter('destination') + "'>");  // This is added to the end of the form and triggers the redirect after the saving is done
    $form.submit(); // Submitting the form saves the data, and then redirects
    });
});**/
	
	
	
});
(function ($, Drupal) {
    /*global jQuery:false */
    /*global Drupal:false */
    
 jQuery(document).ready(function () {
 	  var tf = new TableFilter(document.querySelector('#datatable'), {
    base_path: '/libraries/tablefilter/'
});
tf.init();
  });
    "use strict";
    /**
     * Fix tableheader.js Sticky Table headers overflow-x:scroll issue.
     */
    Drupal.behaviors.tableheader = {
        attach: function (context) {
            var $table = $(".table-responsive");
            if ($table.length == 0) {
                console.log($table.length);
                $table = $("html");
            }
            var $header = $(".sticky-header");
            var $header_left = parseInt($header.css('left'));
            $table.on("scroll", function (e) {
                $header.css('left', $header_left-$table.scrollLeft());
            });
            $(window).on("scroll", function (e) {
                $header.css('left', $header_left-$table.scrollLeft());
            });
        }
    };
})(jQuery, Drupal);

function GetURLParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
        else return false;
    }
}